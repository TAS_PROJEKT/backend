from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from geopy.geocoders import Nominatim

class GeocodingViewSet(viewsets.ViewSet):
    def list(self, request):
        q = self.request.GET.get('q', None)
        if q is None:
            return Response([])

        geolocator = Nominatim()
        location = geolocator.geocode(q, language='pl-PL', exactly_one=True, timeout=5)
        if location:
            result = { 'lat': location.latitude, 'lon': location.longitude }
        return Response(result)

