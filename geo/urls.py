from django.urls import path
from geo.views import GeocodingViewSet

app_name = 'geo'

urlpatterns = [
    path('geocoding/', GeocodingViewSet.as_view({'get': 'list'}), name='geocoding')
]