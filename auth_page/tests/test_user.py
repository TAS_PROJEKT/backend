from django.test import TestCase
from django.contrib.auth import get_user_model
from auth_page.models import User

class UserCreationTest(TestCase):
    def setUp(self):
        self.email = 'example@example.com'
        self.password = 'test'
        self.name = 'test'

        self.data = { 'email': self.email, 'password': self.password }
        self.user = get_user_model()
        self.instance = get_user_model().objects.create_user(**self.data)

    ''' test if user is really custom user, not basic django one'''
    def test_user_instance(self):
        self.assertEqual(self.user, User)

    def test_user_creation(self):
        self.assertEqual(self.user.objects.count(), 1)

    def test_email_param(self):
        self.assertEqual(self.instance.email, self.data['email'])

    '''should work with email and password, not username'''
    def test_bad_creation_exception(self):
        with self.assertRaises(TypeError):
            self.user.objects.create_user(username=self.name, password=self.password)

    def test_superuser_creation(self):
        admin_email = 'admin@example.com'
        self.user.objects.create_superuser(email=admin_email, password=self.password)
        num_of_superusers = self.user.objects.filter(is_staff=True).count()
        self.assertEqual(num_of_superusers, 1)

class UserMethodsTest(TestCase):
    def setUp(self):
        self.email = 'example@example.com'
        self.password = 'test'
        self.name = 'test'

        self.data = {'email': self.email, 'password': self.password, 'name': self.name}
        self.user = get_user_model().objects.create_user(**self.data)

    def test_str(self):
        self.assertEqual(str(self.user), self.user.email)

    def test_get_name(self):
        self.assertEqual(self.user.get_name(), self.data['name'])

    ''' na razie byle co'''
    def test_perms(self):
        self.assertTrue(self.user.has_perm(None))

    def test_module_perms(self):
        self.assertTrue(self.user.has_module_perms(None))
