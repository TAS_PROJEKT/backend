from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth import get_user_model
from io import StringIO

class TryCreateSuperUserTest(TestCase):
    def setUp(self):
        self.out = StringIO()

    def test_if_creates(self):
        call_command('trycreatesuperuser', stdout=self.out)
        self.assertIn('Extended superuser created successfully!', self.out.getvalue())

    def test_when_exists(self):
        get_user_model().objects.create_superuser(email='example@example,com', password='test')
        call_command('trycreatesuperuser', stdout=self.out)
        self.assertIn('Extended superuser already exists in the database!', self.out.getvalue())




