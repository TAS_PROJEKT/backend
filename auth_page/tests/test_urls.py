from django.test import TestCase
from django.urls import reverse

class TestURLs(TestCase):
    def test_token(self):
        self.assertEqual(reverse('auth_obtain_token'), '/api/auth/obtain-token/')

    def test_register(self):
        self.assertEqual(reverse('auth_register'), '/api/auth/register/')

    def test_change_password(self):
        self.assertEqual(reverse('auth_change_password'), '/api/auth/change-password/')

    def test_activate(self):
        uidb64 = 'MTI'
        token = '4z1-2462e6b81dad8a43be10'
        data = { 'uidb64': uidb64, 'token': token }

        self.assertEqual(reverse('auth_activate', kwargs=data), '/api/auth/activate/{}/{}/'.format(uidb64, token))

    def test_user_data(self):
        self.assertEqual(reverse('auth_user_data'), '/api/auth/user-data/')

    def test_logout(self):
        self.assertEqual(reverse('auth_logout'), '/api/auth/logout/')