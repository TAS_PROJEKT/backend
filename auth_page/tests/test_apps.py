from django.apps import apps
from django.test import TestCase
from auth_page.apps import AuthPageConfig

class auth_pageConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(AuthPageConfig.name, 'auth_page')
        self.assertEqual(apps.get_app_config('auth_page').name, 'auth_page')