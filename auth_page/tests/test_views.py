from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.conf import settings
from django.core import mail
import re

class Register(APITestCase):
    def setUp(self):
        self.email = 'example@example.com'
        self.password = 'test'
        self.name = 'Test'
        self.bad_email = 'example@examplecom'
        self.bad_password = ''

        self.url = reverse('auth_register')
        self.data = { 'email': self.email, 'password': self.password, 'name': self.name }
        self.User = get_user_model()
        self.response = self.client.post(self.url, self.data, format='json')
        url = re.search(r'http://.*', str(mail.outbox[0].message()))
        self.activation_link = url.group(0)

    def test_email_sent(self):
        self.assertEqual(len(mail.outbox), 1)

    def test_activation_link(self):
        response = self.client.get(self.activation_link)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_bad_activation_request(self):
        self.User.objects.all().first().delete()
        response = self.client.get(self.activation_link)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_response_status_register(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_response_status_duplicate(self):
        self.client.post(self.url, self.data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(mail.outbox), 2)

    def test_creation(self):
        self.assertEqual(self.User.objects.count(), 1)

    def test_bad_request(self):
        response = self.client.post(self.url, { 'email': self.bad_email, 'password': self.bad_password }, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class TestChangePassword(APITestCase):
    def setUp(self):
        self.email = 'example@example.com'
        self.password = 'test'

        data = { 'email': self.email, 'password': self.password }
        user = get_user_model().objects.create_user(**data)
        self.client.force_authenticate(user=user)

        self.url = reverse('auth_change_password')
        self.change_data = {'old_password': self.password, 'new_password': 'new'}
        self.response = self.client.put(self.url, self.change_data, format='json')

    def test_response_status(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_bad_request(self):
        bad_data = {}
        response = self.client.put(self.url, bad_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class TestLogin(APITestCase):
    def setUp(self):
        self.email = 'example@example.com'
        self.password = 'test'
        self.bad_email = 'example@examplecom'
        self.bad_password = ''

        data = { 'email': self.email, 'password': self.password }
        user = get_user_model().objects.create_user(**data)
        user.is_active = True
        user.save()

        self.url = reverse('auth_obtain_token')
        self.response = self.client.post(self.url, data, format='json')

    def test_response_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_token_return(self):
        self.assertIn('token', self.response.data)

    def test_bad_request(self):
        bad_data = { 'email': self.bad_email, 'password': self.bad_password }
        response = self.client.post(self.url, bad_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class TestUserData(APITestCase):
    def setUp(self):
        self.email = 'example@example.com'
        self.password = '123'
        self.name = 'test'

        user_data = { 'email': self.email, 'password': self.password, 'name': self.name }
        self.user = get_user_model().objects.create_user(**user_data)
        self.client.force_authenticate(user=self.user)
        url = reverse('auth_user_data')
        self.response = self.client.get(url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_response_data(self):
        expected_response = { 'email': self.user.email, 'name': self.user.name }
        self.assertEqual(self.response.data, expected_response)

class TestLogout(APITestCase):
    def setUp(self):
        self.email = 'example@example.com'
        self.password = '123'
        self.name = 'test'

        user_data = { 'email': self.email, 'password': self.password, 'name': self.name }
        self.user = get_user_model().objects.create_user(**user_data)
        self.client.force_authenticate(user=self.user)


    def test_logout(self):
        prev_jwt_key = self.user.jwt_secret_key
        url = reverse('auth_logout')
        self.response = self.client.get(url)
        self.user.refresh_from_db()
        new_jwt_key = self.user.jwt_secret_key
        self.assertTrue(prev_jwt_key != new_jwt_key)


