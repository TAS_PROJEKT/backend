from rest_framework import serializers
from django.contrib.auth import get_user_model
'''todo: walidacja'''
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('email', 'password', 'name', 'id')

    def create(self, validated_data):
        user = get_user_model().objects.create_user(validated_data['email'], validated_data['password'])
        name = validated_data.get('name', None)
        if name:
            user.name = name
            user.save()
        return user

class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
