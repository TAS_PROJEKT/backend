from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin

import uuid

class UserManager(BaseUserManager):
    """
    create a new user

    @param username:  the name for the new user
    @param password:  the password for the new user. if none is provided a random password is generated
    """
    def create_user(self, email, password, **kwargs):
        email = self.normalize_email(email)
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        user = self.create_user(email, password, **kwargs)
        user.is_staff = True
        user.is_active = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    INDIVIDUAL = 'IND'
    OPERATOR = 'OP'

    USER_TYPE_CHOICES = (
        (INDIVIDUAL, 'individual'),
        (OPERATOR, 'operator')
    )

    email = models.EmailField(max_length=255, unique=True, verbose_name='email address')
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateField(auto_now_add=True)
    is_admin = models.BooleanField(default=False)
    is_premium = models.BooleanField(default=False)
    user_type = models.CharField(max_length=10, choices=USER_TYPE_CHOICES, default=INDIVIDUAL)
    owned_by = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL, related_name='accounts')
    jwt_secret_key = models.UUIDField(default=uuid.uuid4)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_name(self):
        return self.name

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True