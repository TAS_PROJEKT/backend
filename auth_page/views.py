from rest_framework.views import APIView
from rest_framework.generics import UpdateAPIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_text
from auth_page.serializers import UserSerializer, ChangePasswordSerializer
from auth_page.tokens import account_activation_token
from company_app.models import Company
from company_app.serializers import CompanySerializer

import uuid

class Register(APIView):
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token = account_activation_token.make_token(user)
                uid = urlsafe_base64_encode(force_bytes(user.id)).decode()
                activation_url = '{}activate/{}/{}/'.format(settings.API_AUTH_URL, uid, token)

                send_mail(settings.REGISTER_EMAIL_SUBJECT,
                          settings.REGISTER_EMAIL_CONTENT.format(activation_url),
                          settings.COMPANY_EMAIL,
                          (user.email,),
                          fail_silently=False)
                return Response(status=status.HTTP_201_CREATED)

        email = serializer.data['email']
        taken = get_user_model().objects.filter(email=email)
        if taken:
            send_mail(settings.EMAIL_TAKEN_SUBJECT,
                      settings.EMAIL_TAKEN_CONTENT,
                      settings.COMPANY_EMAIL,
                      (email,),
                    fail_silently=False)
            return Response(status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET',])
@permission_classes((AllowAny,))
def activate(request, uidb64, token):
    uid = force_text(urlsafe_base64_decode(uidb64))
    try:
        user = get_user_model().objects.get(pk=uid)
    except:
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()

        return Response(status=status.HTTP_200_OK)

    return Response(status=status.HTTP_400_BAD_REQUEST)

class Logout(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user = request.user
        user.jwt_secret_key = uuid.uuid4()
        user.save()
        return Response(status=status.HTTP_200_OK)

class ChangePassword(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer

    def get_object(self, queryset=None):
        return self.request.user

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        user = self.get_object()

        if serializer.is_valid():
            old_password = serializer.data.get('old_password', None)
            if user.check_password(old_password):
                user.set_password(serializer.data.get('new_password'))
            return Response(status.HTTP_200_OK)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

class UserData(APIView):
    serializer_class = CompanySerializer

    def get(self, request):
        user = self.request.user
        try:
            company_query = Company.objects.get(owner=user)
            serializer = self.serializer_class(company_query)
            company = serializer.data
        except Exception as e:
            company = {}
        data = {
            'name': user.name,
            'email': user.email,
            'company': company,
        }
        return Response(data, status.HTTP_200_OK)
