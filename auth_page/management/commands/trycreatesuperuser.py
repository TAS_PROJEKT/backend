from auth_page.models import User
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):

    def handle(self, *arg, **options):
        if User.objects.filter(is_staff=True).count() > 0:
            self.stdout.write('Extended superuser already exists in the database!')
        else:
            try:
                User.objects.get(email='admin@admin.com').save()
            except:
                User.objects.create_superuser(email='admin@admin.com', password='Temp1234')
            self.stdout.write('Extended superuser created successfully!')

