from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from auth_page import views

urlpatterns = [
    path('obtain-token/', obtain_jwt_token, name='auth_obtain_token'),
    path('logout/', views.Logout.as_view(), name='auth_logout'),
    path('register/', views.Register.as_view(), name='auth_register'),
    path('change-password/', views.ChangePassword.as_view(), name='auth_change_password'),
    path('activate/<uidb64>/<token>/', views.activate, name='auth_activate'),
    path('user-data/', views.UserData.as_view(), name='auth_user_data'),
]
