from django.contrib import admin
from company_app.models import Company, CompanyReview

# Register your models here.

admin.site.register(Company)
admin.site.register(CompanyReview)
