from django.db.models import Avg
from rest_framework import serializers
from company_app.models import Company, CompanyReview
from geo.serializers import PointSerializer
from geo.models import Point

class CompanyReviewSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.email')
    company = serializers.ReadOnlyField(source='company.id')
    class Meta:
        model = CompanyReview
        fields = ('id', 'owner', 'company', 'review', 'rating', 'date')

class CompanySerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.email')
    rating = serializers.SerializerMethodField()
    geolocation = PointSerializer(required=False)
    company_review = CompanyReviewSerializer(many=True, required=False)

    class Meta:
        model = Company
        fields = ('id', 'owner', 'company_name', 'company_size', 'location', 'site_url', 'logo', 'rating', 'geolocation',
         'description', 'company_review',)

    #for aggregating - and getting average rating
    #can't be ordered or filtered as it would need to be in database
    def get_rating(self, obj):
        result = CompanyReview.objects.filter(company=obj).aggregate(mean=Avg('rating'))
        return result["mean"]

    def create(self, validated_data):
        geolocation = validated_data.pop('geolocation', None)
        point = Point.objects.create(**geolocation) if geolocation is not None else None
        owner = validated_data.pop('owner')
        company = Company.objects.create(owner=self.context['request'].user, geolocation=point, **validated_data)
        return company

    def update(self, instance, validated_data):
        instance.company_name = validated_data.get('company_name', instance.company_name)
        instance.company_size = validated_data.get('company_size', instance.company_size)
        instance.site_url = validated_data.get('site_url', instance.site_url)
        geolocation = validated_data.get('geolocation', None)
        if geolocation is not None:
            point, created = Point.objects.update_or_create(id=instance.geolocation.id, defaults=geolocation)
            if created:
                instance.geolocation = point
        instance.save()
        return instance

class CompanyLogoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('logo',)


