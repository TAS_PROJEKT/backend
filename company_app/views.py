from rest_framework import viewsets, filters, status
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import detail_route
from django_filters import rest_framework as dj_filters
from company_app.permissions import OwnCompanyPermission, OwnReviewPermission
from company_app.models import Company, CompanyReview
from company_app.serializers import CompanySerializer, CompanyReviewSerializer, CompanyLogoSerializer
from django.shortcuts import get_object_or_404


class CompanyViewSet(viewsets.ModelViewSet):
    """
    Actions provided are `list`, `create`, `retrieve`,
    `update` and `destroy`.

    You need to be registed to post a company.
    You can own only one company at a time.
    You need to be owner to update/destroy a company.
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = (OwnCompanyPermission,)

    filter_backends = (dj_filters.DjangoFilterBackend,
                       filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('id', 'owner__email', 'company_name',
                     'company_size', 'location', 'site_url')
    ordering_fields = ('id', 'owner__email', 'company_name',
                       'company_size', 'location', 'site_url')
    filterset_fields = ('owner', 'company_name', 'company_size', 'location', )

    def perform_create(self, serializer):
        imagesent = self.request.FILES.get('image')
        if imagesent:
            serializer.save(logo=imagesent)
        serializer.save(owner=self.request.user)

    @detail_route(methods=['put'], parser_classes=(MultiPartParser,), serializer_class=CompanyLogoSerializer, url_name='logo')
    def logo(self, request, pk=None):
        company = get_object_or_404(Company, id=pk)
        self.check_object_permissions(request, company)
        serializer = self.get_serializer_class()(
            company, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CompanyReviewViewSet(viewsets.ModelViewSet):
    """
    Actions provided are `list`, `create`, `retrieve`,
    and `destroy`.

    You need to be registed to post a review.
    You can have one review per company at a time.
    You need to be owner to update/destroy a review.
    """
    serializer_class = CompanyReviewSerializer
    permission_classes = (OwnReviewPermission,)
    queryset = CompanyReview.objects.all()

    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('id', 'owner__email',
                     'company__company_name', 'review', 'rating')
    ordering_fields = ('id', 'owner__email',
                       'company__company_name', 'review', 'rating')


    def create(self, request):
        company = request.data.get('company', None)
        if company:
            company = get_object_or_404(Company, id=company)
            review = CompanyReview.objects.create(company=company, owner=request.user)
            serializer = self.get_serializer_class()(review)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class CompanyReviewDetailViewSet(viewsets.ModelViewSet):
    serializer_class = CompanyReviewSerializer
    # permission_classes = (OwnReviewPermission,)
    queryset = CompanyReview.objects.all()

    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('id', 'owner__email',
                     'company__company_name', 'review', 'rating')
    ordering_fields = ('id', 'owner__email',
                       'company__company_name', 'review', 'rating')

    def list(self, request, *args, **kwargs):
        company_id = self.kwargs.get('company_id', None)
        if company_id:
            company = get_object_or_404(Company, pk=company_id)
            query = CompanyReview.objects.filter(company=company)
            serializer = self.get_serializer_class()(query, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        company_id = self.kwargs.get('company_id', None)
        if company_id:
            company = get_object_or_404(Company, id=company_id)
            review = CompanyReview.objects.create(company=company, owner=request.user)
            serializer = self.get_serializer_class()(data=request.data)
            if serializer.is_valid():
                serializer.save(company=company, owner=request.user)
                return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)
