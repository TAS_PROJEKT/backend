from rest_framework import permissions
from company_app.models import Company, CompanyReview

class OwnCompanyPermission(permissions.BasePermission):
    """
    Permission to only allow adding companies for registered users
    """
    def has_permission(self, request, view):
        #only for registered users and only one company per user
        if request.method in ['POST']:
            if request.user.is_anonymous or Company.objects.filter(owner=request.user).exists():
                return False
        return True
    """
    Object-level permission to only allow updating user's own listing
    """
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        # okay if user who issued request is also the object's owner
        return obj.owner == request.user

class OwnReviewPermission(permissions.BasePermission):
    """
    Permission to only allow adding reviews for registered users
    """
    def has_permission(self, request, view):
        #only for registered users and only one review for company per user
        if request.method in ['POST', 'PUT', 'PATCH']:
            company = request.data.get('company', None)
            company_id = view.kwargs.get('company_id', None)
            company_pk = company if company else company_id
            review = CompanyReview.objects.filter(owner=request.user, company=company_pk).exists()
            if review:
                return False
            return True
        return True
    """
    Permission to only allow updating and deleting user's own reviews
    """
    #only for registered users
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        # okay if user who issued request is also the object's owner
        return obj.owner == request.user