from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator

from datetime import datetime

from geo.models import Point

User = get_user_model()

class Company(models.Model):
    allowed_extensions = settings.IMAGE_EXTENSIONS

    company_name = models.CharField(max_length=100, blank=True)
    company_size = models.CharField(max_length=30, blank=True)
    location = models.CharField(max_length=100, blank=True)
    geolocation = models.OneToOneField(Point, null=True, blank=True, on_delete=models.CASCADE)
    site_url = models.URLField(max_length=100, blank=True)
    logo = models.ImageField(upload_to='logos', blank=True, null=True,
                             validators=[FileExtensionValidator(allowed_extensions)])
    owner = models.OneToOneField(User, related_name='company_representative',
                              on_delete=models.CASCADE, blank=True, null=True, default=None)
    description = models.TextField(blank=True, null=True)

    REQUIRED_FIELDS = ['owner', 'company_name']

    class Meta:
        verbose_name_plural = 'companies'

    def __str__(self):
        return self.company_name

class CompanyReview(models.Model):
    review = models.TextField(blank=True)
    rating = models.IntegerField(choices=zip(range(1, 6), range(1, 6)), default=3)
    date = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(Company, related_name='company_review',
                                on_delete=models.CASCADE, blank=True, null=True, default=None)
    owner = models.ForeignKey(User, related_name='company_reviewer',
                              on_delete=models.CASCADE, blank=True, null=True, default=None)

    REQUIRED_FIELDS = ['company', 'rating']
