from django.urls import path
from rest_framework.routers import DefaultRouter
from company_app import views

router = DefaultRouter()
router.register(r'companies', views.CompanyViewSet, base_name='companies')
router.register(r'reviews', views.CompanyReviewViewSet, base_name='reviews')
router.register(r'(?P<company_id>\d+)/reviews', views.CompanyReviewDetailViewSet, base_name='detail_reviews')

urlpatterns = router.urls
