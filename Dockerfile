FROM python:3.7.0-alpine3.8

LABEL maintainer="apiotrowski312@gmail.com"

COPY ./image/runserver /usr/local/bin/runserver

RUN apk add --update bash \
    postgresql-dev \
    gcc \
    python3-dev \
    musl-dev \
    bash \
    jpeg-dev \
    zlib-dev \
    freetype-dev \
    ttf-freefont

COPY ./requirements.txt ./
RUN pip3 install -r requirements.txt

WORKDIR /app

VOLUME ["/app"]

EXPOSE 8000