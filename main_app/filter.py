from django_filters import rest_framework as filters
from main_app.models import JobListing

class JobListingFilter(filters.FilterSet):
    class Meta:
        model = JobListing
        fields = {
            'company__company_name': ['icontains'], 
            'company__location': ['icontains'],
            'description': ['icontains'],
            'created': ['lt', 'gt'],
            'salary_low': ['lt', 'gt'],
            'salary_high': ['lt', 'gt'],
            'title': ['icontains'],
            'overall_experience': ['exact'],
            'tools__tool': ['icontains'],
        }