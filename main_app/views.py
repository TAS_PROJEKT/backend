import logging
from rest_framework import viewsets, filters, status
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from django_filters import rest_framework as dj_filters
from django.shortcuts import get_object_or_404
from main_app.permissions import OwnProfilePermission, OwnListingPermission, OwnApplicationPermission
from main_app.models import JobListing, ToolExperience, Application
from main_app.serializers import JobListingSerializer, ToolExperienceSerializer, ApplicationSerializer, ApplicationFileSerializer, ToolbarSerializer
from main_app.filter import JobListingFilter
from company_app.models import Company


class JobListingViewSet(viewsets.ModelViewSet):
    """
    Actions provided are `list`, `create`, `retrieve`,
    `update` and `destroy`.
    You need to be registered and own a company to create a listing.
    You need to be owner to update/destroy a listing.
    """
    queryset = JobListing.objects.all()
    serializer_class = JobListingSerializer
    permission_classes = (OwnProfilePermission,)

    filter_backends = (dj_filters.DjangoFilterBackend,
                       filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('title', 'company__company_name', 'company__location',
                     'description', 'created', 'salary_low', 'salary_high', 'tools__tool')
    ordering_fields = ('id', 'title', 'company__company_name',
                       'company__location', 'created', 'salary_low', 'salary_high')
    filterset_class = JobListingFilter

    def get_queryset(self):
        queryset = JobListing.objects.all()

        tool_id = self.request.GET.get('tool', None)
        if tool_id and tool_id != '1':
            tool = get_object_or_404(ToolExperience, id=tool_id)
            queryset = queryset.filter(tools__tool=tool.tool)
        location = self.request.GET.get('location', None)
        if location:
            queryset = queryset.filter(company__location__icontains=location)

        experience = self.request.GET.get('experience', None)
        if experience:
            queryset = queryset.filter(overall_experience=experience)

        company = self.request.GET.get('company', None)
        if company:
            queryset = queryset.filter(company_id=company)

        salary = self.request.GET.get('salary', None)
        if salary:
            queryset = queryset.filter(salary_low__gte=salary)

        return queryset


class CompanyJobListingViewSet(viewsets.ModelViewSet):
    queryset = JobListing.objects.all()
    serializer_class = JobListingSerializer

    def get_queryset(self):
        company = Company.objects.filter(owner=self.request.user).first()
        if company:
            return JobListing.objects.filter(company=company)
        return None


class ToolbarViewSet(viewsets.ModelViewSet):
    serializer_class = ToolbarSerializer
    permission_classes = (AllowAny,)
    queryset = ToolExperience.objects.filter(id__lt=11)


class ToolExperienceViewSet(viewsets.ModelViewSet):
    """
    Actions provided are `list`, `create`, `retrieve`,
    `update` and `destroy`.

    You need to be listing's owner to create/update/destroy.
    """
    serializer_class = ToolExperienceSerializer
    permission_classes = (OwnListingPermission,)

    filter_backends = (dj_filters.DjangoFilterBackend,
                       filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('tool', 'experience')
    ordering_fields = ('id', 'tool', 'experience')
    filterset_fields = ('tool',)

    def get_queryset(self):
        job_id = self.kwargs['job_id']
        return ToolExperience.objects.filter(job=job_id)

    def perform_create(self, serializer):
        job_query = JobListing.objects.get(id=self.kwargs['job_id'])
        serializer.save(job=job_query)


class ApplicationViewSet(viewsets.ModelViewSet):
    """
    Actions provided are `list`, `create`, `retrieve`,
    and `destroy`.

    Anyone can create one, being registered is optional.
    You need to be listing's owner to see relevant applications,
    and be able to delete them.
    """
    serializer_class = ApplicationSerializer
    permission_classes = (OwnApplicationPermission,)

    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'description', 'user')
    ordering_fields = ('id', 'name', 'user')

    def create(self, request):
        user = self.request.user
        job_pk = request.data.get('job', None)
        job = get_object_or_404(JobListing, id=job_pk)
        app = Application.objects.create(user=self.request.user, job=job)
        serializer = self.get_serializer_class()(app)
        return Response(serializer.data, status=status.HTTP_200_OK)


    def list(self, request):
        company = self.request.GET.get('company', None)
        if company is None:
            query = Application.objects.all()
            serializer = self.serializer_class(query, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        jobs = JobListing.objects.filter(company__id=company)
        query = Application.objects.filter(job__in=jobs)
        serializer = self.serializer_class(
            Application.objects.filter(job__in=jobs), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @detail_route(methods=['put'], url_name='cv',
                  serializer_class=ApplicationFileSerializer, parser_classes=(MultiPartParser,))
    def cv(self, request, pk=None):
        obj = get_object_or_404(Application, id=pk)
        serializer = self.get_serializer_class()(
            obj, data=request.data, partial=True)
        self.check_object_permissions(request, obj)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['put'], url_name='accept')
    def accept(self, request, pk=None):
        app = get_object_or_404(Application, id=pk)
        serializer = ApplicationSerializer(
            app, data=request.data, partial=True)
        self.check_object_permissions(self.request, app)
        if serializer.is_valid():
            app.accepted = serializer.validated_data['accepted']
            app.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class JobApplicationViewSet(viewsets.ModelViewSet):
    serializer_class = ApplicationSerializer
    permission_classes = (OwnApplicationPermission,)
    queryset = JobListing.objects.all()

    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'description', 'user')
    ordering_fields = ('id', 'name', 'user')

    def list(self, request, *args, **kwargs):
        job_id = kwargs.get('job_id', None)
        if job_id:
            job = get_object_or_404(JobListing, id=job_id)
            query = Application.objects.filter(job=job)
            serializer = self.get_serializer_class()(query, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        job_id = kwargs.get('job_id', None)
        if job_id:
            job = get_object_or_404(JobListing, id=job_id)
            serializer = self.get_serializer_class()(data=request.data)
            if serializer.is_valid():
                serializer.save(user=request.user, job=job)
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_400_BAD_REQUEST)
