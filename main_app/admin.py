from django.contrib import admin
from main_app.models import JobListing, ToolExperience, Application

class ToolExperienceInline(admin.TabularInline):
    model = ToolExperience
    extra = 3

class JobListingAdmin(admin.ModelAdmin):
    inlines = [
        ToolExperienceInline,
     ]

admin.site.register(JobListing, JobListingAdmin)
admin.site.register(ToolExperience)
admin.site.register(Application)
