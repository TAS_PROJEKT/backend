from django.urls import path
from rest_framework.routers import DefaultRouter
from main_app import views

router = DefaultRouter()
router.register(r'tools', views.ToolbarViewSet, base_name='toolbar')
router.register(r'jobs', views.JobListingViewSet, base_name='jobs')
router.register(r'jobs/(?P<job_id>\d+)/tools',
                views.ToolExperienceViewSet, base_name='tools')
router.register(r'applications', views.ApplicationViewSet,
                base_name='applications')
router.register(r'company-jobs', views.CompanyJobListingViewSet,
                base_name='company_jobs')
router.register(r'jobs/(?P<job_id>\d+)/applications', views.JobApplicationViewSet, base_name='job_applications')

urlpatterns = router.urls
