from PIL import Image, ImageDraw, ImageFont
import random

def pick_random_color():
    green = (13, 155, 22)
    red = (102, 0, 0)
    blue = (0, 102, 102)
    navy = (0, 0, 153)
    purple = (51, 0, 25)
    pink = (255, 153, 153)
    some = (0, 51, 51)
    yellow = (255, 255, 0)
    colors = [green, red, blue, navy, purple, pink, some, yellow]
    return colors[random.randint(0, len(colors)-1)]

def generate_image(letter):
    img = Image.new('RGB', (30, 30), color='white')
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype("/usr/share/fonts/TTF/FreeMono.ttf", 30)
    draw.ellipse((0, 0, 30, 30), fill=pick_random_color())
    draw.text((7, 0), letter.upper(), font=font, fill=(255, 255, 255))
    return img
