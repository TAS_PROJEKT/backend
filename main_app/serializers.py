from rest_framework import serializers
from main_app.models import JobListing, ToolExperience, Application
from company_app.models import Company
from company_app.serializers import CompanySerializer


class ToolExperienceSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(required=False)

    class Meta:
        model = ToolExperience
        fields = ('id', 'tool', 'experience')


class ToolbarSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToolExperience
        fields = ('id', 'tool', 'icon')


class JobListingSerializer(serializers.ModelSerializer):
    tools = ToolExperienceSerializer(many=True)
    company = CompanySerializer(read_only=True)

    class Meta:
        model = JobListing
        fields = ('id', 'title', 'description', 'created', 'salary_low', 'salary_high',
                  'overall_experience', 'contact_email', 'tools', 'company',
                  'salary_high_currency', 'employment_type')

    def create(self, validated_data):
        tools = validated_data.pop('tools', None)
        job_listing = JobListing.objects.create(
            owner=self.context['request'].user, **validated_data)
        if tools:
            for tool in tools:
                ToolExperience.objects.create(
                    job=job_listing, tool=tool['tool'], experience=tool['experience'])

        user_company = Company.objects.filter(
            owner=self.context['request'].user).first()
        if user_company:
            job_listing.company = user_company
            job_listing.save()
        return job_listing

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.created = validated_data.get('created', instance.created)
        instance.salary_high = validated_data.get(
            'salary_high', instance.salary_high)
        instance.salary_low = validated_data.get(
            'salary_low', instance.salary_low)
        instance.overall_experience = validated_data.get(
            'overall_experience', instance.overall_experience)
        instance.contact_email = validated_data.get(
            'contact_email', instance.contact_email)
        instance.salary_high_currency = validated_data.get(
            'salary_high_currency', instance.salary_high_currency)

        tools = validated_data.pop('tools', None)
        if tools:
            for tool in tools:
                id = tool.pop('id', None)
                obj, created = ToolExperience.objects.update_or_create(
                    id=id, defaults=tool)
                if created:
                    obj.job = instance
                    obj.save()
        instance.save()
        return instance


class ApplicationSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.email')
    job = serializers.ReadOnlyField(source='job.title')

    class Meta:
        model = Application
        fields = ('id', 'name', 'comment', 'cv', 'user', 'job', 'accepted')


class ApplicationFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ('cv',)
