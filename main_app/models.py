from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from djmoney.models.fields import MoneyField
from django.core.validators import FileExtensionValidator
from company_app.models import Company

User = get_user_model()


class JobListing(models.Model):

    EXPERIENCE_CHOICES = (
        ('jr', 'Junior'),
        ('md', 'Mid'),
        ('sr', 'Senior'),
    )

    EMPLOYMENT_TYPES = (
        ('perm', 'Permanent'),
        ('b2b', 'B2B'),
    )

    allowed_extensions = settings.IMAGE_EXTENSIONS

    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    salary_low = MoneyField(max_digits=10, decimal_places=0,
                            default_currency=settings.DEFAULT_CURRENCY, blank=True)
    salary_high = MoneyField(max_digits=10, decimal_places=0,
                             default_currency=settings.DEFAULT_CURRENCY, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    overall_experience = models.CharField(
        choices=EXPERIENCE_CHOICES, max_length=2, default='jr')
    contact_email = models.EmailField(max_length=70, blank=True)
    # PK to main company model
    company = models.ForeignKey(Company, related_name='company_job',
                                on_delete=models.CASCADE, blank=True, null=True, default=None)
    # not exacly needed, but should stay for ease of access in permissions and
    # such
    owner = models.ForeignKey(User, related_name='representative',
                              on_delete=models.CASCADE, blank=True, null=True, default=None)
    employment_type = models.CharField(
        choices=EMPLOYMENT_TYPES, default='perm', max_length=64)

    REQUIRED_FIELDS = ['title']

    def get_name(self):
        return self.title

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('created',)


class ToolExperience(models.Model):

    tool = models.CharField(max_length=30, blank=True)
    experience = models.IntegerField(
        choices=zip(range(1, 6), range(1, 6)), default=3)
    job = models.ForeignKey(JobListing, related_name='tools',
                            on_delete=models.CASCADE, blank=True, null=True, default=None)
    icon = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.tool


class Application(models.Model):
    name = models.CharField(max_length=50, blank=True)
    comment = models.TextField(blank=True)
    cv = models.FileField(upload_to='files', null=True,
                          validators=[FileExtensionValidator(settings.CV_EXTENSIONS)])
    job = models.ForeignKey(JobListing, related_name='target_job',
                            on_delete=models.CASCADE, blank=True, null=True, default=None)
    user = models.ForeignKey(User, related_name='user_account',
                             on_delete=models.CASCADE, blank=True, null=True)
    accepted = models.NullBooleanField(default=None)

    REQUIRED_FIELDS = ['name']

    def __str__(self):
        return self.name
