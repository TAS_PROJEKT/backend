from rest_framework import permissions
from main_app.models import JobListing, Application
from company_app.models import Company

class OwnProfilePermission(permissions.BasePermission):
    """
    Permission to only allow adding listings when registed and with company
    """
    def has_permission(self, request, view):
        #only for registered users and with company
        if request.method in ['POST']:
            if request.user.is_anonymous or not Company.objects.filter(owner=request.user).exists():
                return False
            else:
                return True
        else:
            return True
    """
    Object-level permission to only allow updating user's own listing
    """
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        # okay if user who issued request is also the object's owner
        return obj.owner == request.user

class OwnListingPermission(permissions.BasePermission):
    """
    Permission to only allow adding and updating required tools on user's own listings
    """
    def has_permission(self, request, view):
        job_id = view.kwargs.get('job_id', None)
        if job_id:
            job_query = JobListing.objects.get(id=job_id)
            # okay if user who issued request is also the listing's owner
            return job_query.owner == request.user

        if request.method in permissions.SAFE_METHODS:
            return True


class OwnApplicationPermission(permissions.BasePermission):
    """
    Permission to only posting to anyone, and viewing and deleting only for JobListing's owner
    """
    def has_permission(self, request, view):
        job = request.data.get('job', None)
        job_id = view.kwargs.get('job_id', None)
        if job or job_id:
            job_pk = job if job else job_id
            if request.method == 'POST':
                return Application.objects.filter(user=request.user, job_id=job_pk).count() == 0
        return True

    def has_object_permission(self, request, view, obj):
        if view.action == ['accept', 'cv']:
            return True
        if request.method in ['PUT', 'PATCH', 'DELETE']:
            return request.user == obj.user
        if request.method in permissions.SAFE_METHODS:
            return True
        return False
