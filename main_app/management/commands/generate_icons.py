from django.core.management.base import BaseCommand

from io import BytesIO
from django.core.files import File

from main_app.helpers import generate_image
from main_app.models import ToolExperience

class Command(BaseCommand):
    def handle(self, *args, **options):
        tools = ToolExperience.objects.all()
        for tool in tools:
            if not tool.icon:
                icon = generate_image(tool.tool[0])
                blob = BytesIO()
                icon.save(blob, 'JPEG')
                tool.icon.save('{}.jpg'.format(tool.tool), File(blob), save=True)